//
//  SCLeftTransition.m
//  TransitioningExample
//
//  Created by eugene on 04.12.13.
//  Copyright (c) 2013 Double Encore. All rights reserved.
//

#import "SCLeftTransition.h"

@implementation SCLeftTransition
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.5;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    

    
    // 1. obtain state from the context
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    CGRect finalFrame = [transitionContext finalFrameForViewController:toViewController];
    
    // 2. obtain the container view
    UIView *containerView = [transitionContext containerView];
    
    // 3. set initial state
    CGRect screenBounds = [[UIScreen mainScreen] bounds]; toViewController.view.frame =
    CGRectOffset(finalFrame, screenBounds.size.width, 0);
    
    // 4. add the view
    [containerView addSubview:toViewController.view];
    
    // 5. animate
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    [UIView animateWithDuration:duration animations:^{
        
        toViewController.view.frame = finalFrame;
        
    } completion:^(BOOL finished) {
        
        // 6. inform the context of completion
        [transitionContext completeTransition:YES];
        
    }];
    
}
@end

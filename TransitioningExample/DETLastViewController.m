//
//  DETLastViewController.m
//  TransitioningExample
//
//  Created by eugene on 04.12.13.
//  Copyright (c) 2013 Double Encore. All rights reserved.
//

#import "DETLastViewController.h"


@interface DETLastViewController ()

@end

@implementation DETLastViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //  self.view.backgroundColor = [UIColor grayColor];
    
    //  [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized)]];
}


- (void)tapRecognized
{
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)toroot:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)self.navigationController;
    [navigationController popToRootViewControllerAnimated:YES];
}
@end


//
//  SCInvertedBottomTransition.m
//  TransitioningExample
//
//  Created by eugene on 04.12.13.
//  Copyright (c) 2013 Double Encore. All rights reserved.
//

#import "SCInvertedBottomTransition.h"

@implementation SCInvertedBottomTransition
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.5;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{

    
    // 1. obtain state from the context
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    CGRect finalFrame = [transitionContext finalFrameForViewController:toViewController];
    
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    
    // 2. obtain the container view
    UIView *containerView = [transitionContext containerView];
    
//    // 3. set initial state
//    CGRect screenBounds = [[UIScreen mainScreen] bounds]; toViewController.view.frame =
//    CGRectOffset(finalFrame, 0, -screenBounds.size.height);
    CGRect screenBounds = [[UIScreen mainScreen] bounds];

    
    
//    // 4. add the view
//    [containerView addSubview:toViewController.view];
     [containerView addSubview:toViewController.view];
     [containerView addSubview:fromViewController.view];
    // 5. animate
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    [UIView animateWithDuration:duration animations:^{
        
        fromViewController.view.frame =
        CGRectOffset(finalFrame, 0, screenBounds.size.height);
        
    } completion:^(BOOL finished) {
        
        // 6. inform the context of completion
        // 4. add the view
        //[containerView addSubview:toViewController.view];
        [transitionContext completeTransition:YES];
        
    }];
    
}

@end

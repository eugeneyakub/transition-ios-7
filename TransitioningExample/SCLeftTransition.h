//
//  SCLeftTransition.h
//  TransitioningExample
//
//  Created by eugene on 04.12.13.
//  Copyright (c) 2013 Double Encore. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLeftTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end

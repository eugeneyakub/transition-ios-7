//
//  DETViewController.m
//  TransitioningExample
//
//  Created by Brad Dillon on 9/17/13.
//  Copyright (c) 2013 Double Encore. All rights reserved.
//

#import "DETViewController.h"
#import "DETOtherViewController.h"

#import "SCNavControllerDelegate.h"

@interface DETViewController ()
@property (nonatomic, strong) SCNavControllerDelegate *sometransitioningDelegate;

@end

@implementation DETViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
  //  self.view.backgroundColor = [UIColor whiteColor];
    self.sometransitioningDelegate = [SCNavControllerDelegate new];
    self.navigationController.delegate = self.sometransitioningDelegate;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   // DETOtherViewController *other = (DETOtherViewController *)segue.destinationViewController;
   // other.transitioningDelegate = self.transitioningDelegate;
}


@end
